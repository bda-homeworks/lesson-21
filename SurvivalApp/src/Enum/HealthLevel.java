package Enum;

public enum HealthLevel {
    FULL(100),
    MEDIUM(60),
    LOW(30),
    DEAD(1);
    private int health;

    HealthLevel(int i) {
        health = i;
    }
}
