package Services;

import Model.Health;
import Model.HealthCheck;
import Model.HealthHit;

import java.util.Scanner;


public class MainService{
    public static void main(String[] args) {
        Health health = new Health();
        HealthHit thread = new HealthHit(health);
        HealthCheck thread1 = new HealthCheck(health);
        Scanner input = new Scanner(System.in);
        thread1.start();
        thread.start();
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("-----------------------------------");
            System.out.println("[1]. Eat meal (+9%)\n" +
                    "[2]. Drink milk (+8%)\n" +
                    "[3]. Show energy\n" +
                    "[4]. Leave the game\n");
            int choice = input.nextInt();
            switch (choice) {
                case 1:
                    health.setHealth(Health.eatMeal(health.getHealth()));
                    break;
                case 2:
                    health.setHealth(Health.drinkMilk(health.getHealth()));
                    break;
                case 3:
                    System.out.println("Health: " + health.getHealth());
                    break;
                case 4:
                    System.out.println("Leaving the system...");
                    continueLoop = false;
                    break;
                default:
                    System.out.println("No such choice.....");
                    break;
            }
        }
    }
}
