package Model;

public class HealthHit extends Thread{
    private Health health;

    public HealthHit(Health health) {
        this.health = health;
    }

    public void run() {
        while (health.getHealth() > 0) {
            health.decreaseHealth(15);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
