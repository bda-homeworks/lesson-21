package Model;

public class Health{
    private int health;

    public Health() {
        this.health = 100;
    }

    public int getHealth() {
        return health;
    }

    public int setHealth(int health) {
        this.health = health;
        return health;
    }
    public static int eatMeal(int health) {
        health = health + 9;
        return health;
    }
    public static int drinkMilk (int health) {
        health = health + 8;
        return health;
    }
    public void decreaseHealth (int amount) {
        health = Math.max(0, health - amount);
    }
}
