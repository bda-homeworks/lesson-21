package Model;
import Enum.HealthLevel;

import java.awt.*;
import java.io.File;
import java.io.IOException;


public class HealthCheck extends Thread{
    private Health health;

    public HealthCheck (Health health) {
        this.health = health;
    }
    @Override
    public void run () {
        while (health.getHealth() > 0) {
            int currentHealth = health.getHealth();
            HealthLevel healthLevel = HealthLevel.FULL;
            if (currentHealth >= 80) {
                healthLevel = HealthLevel.FULL;
            } else if (currentHealth >= 60) {
                healthLevel = HealthLevel.MEDIUM;
            } else if (currentHealth >= 30) {
                healthLevel = HealthLevel.LOW;
            } else if (currentHealth >= 1) {
                healthLevel = HealthLevel.DEAD;
            }

            switch (healthLevel) {
                case FULL: {
                    System.out.println("Health is FULL: " + currentHealth +"%" );
                    break;
                }
                case MEDIUM: {
                    System.out.println("Health is medium: " + currentHealth +"%" );
                    break;
                }
                case LOW: {
                    System.out.println("You're starving to death bruv get some food rn.: " + currentHealth +"%" );
                    break;
                }
                case DEAD: {
                    System.out.println("You poor bastard, you're dead!: " + currentHealth +"%" );
                    String imageFilePath = "C:\\Users\\xEnakil\\IdeaProjects\\lesson-21\\SurvivalApp\\src\\EREcLxOXsAEZadU.jpg";
                    if (Desktop.isDesktopSupported()) {
                        Desktop desktop = Desktop.getDesktop();
                        File imageFile = new File(imageFilePath);

                        if (imageFile.exists()) {
                            try {
                                desktop.open(imageFile);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            System.out.println("Image file not found: " + imageFilePath);
                        }
                    } else {
                        System.out.println("Desktop is not supported on this platform.");
                    }
                    System.exit(0);
                    break;
                }
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
